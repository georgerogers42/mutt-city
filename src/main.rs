extern crate muttblog;

use std::env;
use muttblog::app;

fn main() {
    let port = env::var("PORT").unwrap_or("8080".to_string());
    let on = format!("0.0.0.0:{}", port);
    println!("Starting on: {}", on);
    app::start(&on);
}
